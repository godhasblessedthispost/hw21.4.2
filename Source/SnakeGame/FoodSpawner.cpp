// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"
#include "Food.h"
#include "ctime"

using namespace::std;

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	FieldGen();
	FoodSpawn();
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::FieldGen()
{
	for (float i = -500.f; i < 500; i += 50)
	{
		Field.Add(i);
	}
}

float AFoodSpawner::Rand()
{
	srand(time(NULL));
	random_shuffle(begin(Field), end(Field));
	return Field[0];
}

void AFoodSpawner::FoodSpawn()
{
	FVector NewLocation(Rand(), Rand(), 20);
	FTransform NewTransform(NewLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}