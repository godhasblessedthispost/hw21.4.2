// Fill out your copyright notice in the Description page of Project Settings.


#include "MapBase.h"

// Sets default values
AMapBase::AMapBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMapBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMapBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMapBase::ArrGen()
{
}

